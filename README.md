# Integer Syndrome Decoding Problem in the Presence of Noise

### Pierre-Louis Cayrel, Brice Colombier, Vlad-Florin Drăgoi, Vincent Grosso

Presented at CBCrypto 2022  
Slides and article provided.

## Usage

### Run the experiments

```terminal
python3 score_binomial_centered_noise.py nb_reps
```

with:
- `nb_reps`: the number of repetitions for each set of parameters

This will store the results in a [dill](https://dill.readthedocs.io/en/latest/) file called `binomial_centered_noise.dill`

### Plot the results

```terminal
python3 plot.py
```

This will plot the results after loading them from the  `binomial_centered_noise.dill` file.
