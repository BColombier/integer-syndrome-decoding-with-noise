from cycler import cycler
import dill
from matplotlib import pyplot as plt
import numpy as np

plt.rcParams["font.size"] = 7
plt.rcParams["text.usetex"] = True
plt.rcParams["font.family"] = "serif"
plt.rcParams["text.latex.preamble"] = r"\usepackage{bm}"
color = plt.cm.viridis(np.linspace(0, 1, 4))
hexcolor = [f"#{int(r*255):02x}{int(g*255):02x}{int(b*255):02x}" for r, g, b, _ in color]
plt.rcParams['axes.prop_cycle'] = cycler(color=hexcolor)


CM_parameters = [(3488, 2720, 64),
                 (4608, 3360, 96),
                 (6688, 5024, 128),
                 (8192, 6528, 128)]


with open(f"binomial_centered_noise.dill", "rb") as log_file:
    results = dill.load(log_file)
plt.figure(figsize=(6.2, 2))
for subplot, (n, k, t) in enumerate(CM_parameters, start=1):
    plt.subplot(1, 4, subplot)
    plt.title(f"{n=} {k=} {t=}")
    for noise, noise_txt in zip([t // 4, t // 2, 3 * t // 4, t],
                                ["$B$(t/4, 1/2)", "$B$(t/2, 1/2)", "$B$(3t/4, 1/2)", "$B$(t, 1/2)"]):
        plt.plot([ratio for ratio in results[n][noise]],
                 [results[n][noise][ratio] for ratio in results[n][noise]],
                 label=f"{noise_txt}")
    plt.xlim((0.3, 1))
    plt.xticks([0.4, 0.6, 0.8, 1])
    plt.fill_between([0, 1],
                     t,
                     t - 3,
                     linewidth=0,
                     alpha=0.25,
                     color="green")
    plt.ylim((t - 27, t))
    if subplot == 1:
        plt.ylabel("Number of ones in\nthe first $n-k$ positions")
        plt.legend(title="Noise param.",
                   loc="lower right",
                   facecolor="white",
                   borderaxespad=0,
                   framealpha=1,
                   labelspacing=0.25,
                   bbox_to_anchor=(1, 0),
                   fancybox=False,
                   edgecolor="black")
    plt.grid()
plt.gcf().supxlabel("Ratio of syndrome entries considered")
plt.tight_layout(w_pad=0)
plt.savefig(f"binomial_centered_noise.pdf")
