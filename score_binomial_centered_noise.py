from argparse import ArgumentParser
from collections import defaultdict
from cycler import cycler
import dill
import numpy as np
from tqdm import tqdm



def get_Hs(n, k, reps):
    H_bytes = np.random.randint(0, np.iinfo(np.uint64).max,
                          size=(n - k) * (n // 8) * reps // 8,
                          dtype=np.uint64).view(np.uint8)
    H_bin = np.reshape(np.unpackbits(H_bytes), (n - k, n, reps)).astype(np.float32)
    return H_bin


def score(H, n, k, t, s, ratio=1.0):
    if ratio != 1:
        H = H[:int((n - k) * ratio), :]
        s = s[:int((n - k) * ratio)]
    H_c = 1 - H
    s_c = t - s
    phi = np.dot(s, H) + np.dot(s_c, H_c)
    return np.argsort(phi)


def count_ones(e, perm, t):
    return np.sum(e[perm][-(n - k):])


parser = ArgumentParser(description="Compute score function for different levels of noise and ratios of syndrome entries")
parser.add_argument("nb_reps", type=int, help="Number of repetitions for each set of parameters")
args = parser.parse_args()

CM_parameters = [(3488, 2720, 64),
                 (4608, 3360, 96),
                 (6688, 5024, 128),
                 (8192, 6528, 128)]
ratios = np.linspace(0.2, 1.0, 9)

results = defaultdict(lambda: defaultdict(lambda: defaultdict(int)))

for (n, k, t) in CM_parameters:
    print(f"{n=:<4} {k=:<4} {t=}")
    e = np.hstack((np.ones(t, dtype=np.float32),
                   np.zeros(n - t, dtype=np.float32)))
    Hs = get_Hs(n, k, args.nb_reps)
    noises = [t // 4, t // 2, 3 * t // 4, t]
    with tqdm(total=len(noises) * len(ratios) * args.nb_reps) as progress_bar:
        for noise in noises:
            for ratio in ratios:
                for rep in range(args.nb_reps):
                    H = Hs[:, :, rep]
                    np.random.shuffle(e)
                    s = np.matmul(H, e)
                    # Add noise
                    s += np.random.binomial(noise, 0.5)
                    s -= noise // 2
                    perm = score(H, n, k, t, s, ratio)
                    results[n][noise][ratio] += count_ones(e, perm, t)
                    progress_bar.update(1)
                results[n][noise][ratio] /= args.nb_reps
with open(f"binomial_centered_noise.dill", "wb") as log_file:
    dill.dump(results, log_file)
